///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file long.h
/// @version 1.0
///
/// Print the characteristics of the "long" and "unsigned long" datatypes.
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date  24_JAN_2021 
///////////////////////////////////////////////////////////////////////////////

extern void doLong();            /// Prlong the characteristics of the "long" datatype
extern void flowLong();          /// Prlong the overflow/underflow characteristics of the "long" datatype

extern void doUnsignedLong();    /// Prlong the characteristics of the "unsigned long" datatype
extern void flowUnsignedLong();  /// Prlong the overflow/underflow characteristics of the "unsigned long" datatype

